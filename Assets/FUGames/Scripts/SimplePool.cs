﻿using System.Collections.Generic;
using UnityEngine;

namespace FUGames.Pooling
{
    public class SimplePool : MonoBehaviour
    {
        [SerializeField] private PoolObject _poolable;
        [SerializeField] private int _count = 10;

        private Queue<PoolObject> _pool = new Queue<PoolObject>();

        private void Start()
        {
            for (int i = 0; i < _count; i++)
            {
                Create();
            }
        }

        public PoolObject GetObject()
        {
            if (_pool.Count == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    Create();
                }
            }

            PoolObject poolable = _pool.Dequeue();
            poolable.transform.parent = null;
            poolable.gameObject.SetActive(true);
            return poolable;
        }

        public void PutObject(PoolObject poolable)
        {
            Transform transform = poolable.transform;
            transform.position = this.transform.position;
            transform.parent = this.transform;
            poolable.gameObject.SetActive(false);
            _pool.Enqueue(poolable);
        }

        private void Create()
        {
            var poolable = Instantiate(
                        _poolable,
                        transform.position,
                        _poolable.transform.rotation,
                        transform);
            poolable.gameObject.SetActive(false);
            _pool.Enqueue(poolable);
        }
    }
}
