﻿namespace FUGames.Pooling
{
    public abstract class PoolObjectData
    {
        public PoolManager PoolManager { get; private set; }

        public PoolObjectData(PoolManager poolManager)
        {
            PoolManager = poolManager;
        }
    }
}
