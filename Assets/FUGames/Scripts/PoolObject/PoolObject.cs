﻿using UnityEngine;

namespace FUGames.Pooling
{ 
    public abstract class PoolObject : MonoBehaviour
    {
        private PoolManager _poolManager;

        public virtual void Init(PoolObjectData data)
        {
            _poolManager = data.PoolManager;
        }

        public virtual void BackToPool()
        {
            _poolManager.Put(GetType(), this);
        }
    }
}