﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FUGames.Pooling
{
    public class PoolManager : MonoBehaviour
    {
        [SerializeField] private PoolParams[] _paramsArray;

        private Dictionary<Type, Pool> _pools;

        private void Awake()
        {
            _pools = new Dictionary<Type, Pool>();

            foreach (var param in _paramsArray)
            {
                var type = param.Template.GetType();
                _pools.Add(type, new Pool(param.Template, param.StartSize, transform));
            }
        }

        public PoolObject Take(Type type)
        {
            if (_pools.ContainsKey(type))
            {
                return _pools[type].TakeObject();
            }

            return null;
        }

        public void Put(Type type, PoolObject poolable)
        {
            if (_pools.ContainsKey(type))
            {
                _pools[type].PutObject(poolable);
            }
        }
    }
}
