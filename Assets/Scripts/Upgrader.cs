﻿using UI;
using UnityEngine;

public class Upgrader : MonoBehaviour
{
    [SerializeField] private Wallet _wallet;
    [SerializeField] private UpgradeButton _strength;
    [SerializeField] private UpgradeButton _health;

    private const string PlayerStrength = "PlayerStrength";
    private const string PlayerHealth = "PlayerHealth";

    public void UpgradeStrength()
    {
        Upgrade(PlayerStrength, _strength);
    }

    public void UpgradeHealth()
    {
        Upgrade(PlayerHealth, _health);
    }

    public void Refresh()
    {
        _strength.CheckInteractivity();
        _health.CheckInteractivity();

        gameObject.SetActive(true);
    }

    private void Upgrade(string parameter, UpgradeButton button)
    {
        var rate = PlayerPrefs.HasKey(parameter) ? PlayerPrefs.GetInt(parameter) : 1;
        var price = Economic.GetUpgradePrice(rate);

        PlayerPrefs.SetInt(parameter, ++rate);
        PlayerPrefs.Save();

        _wallet.RemoveCoins(price);
        button.UpdateValues(rate);
    }
}