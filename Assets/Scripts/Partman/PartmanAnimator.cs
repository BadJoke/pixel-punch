﻿using System;
using UnityEngine;

namespace Partman
{
    public class PartmanAnimator : MonoBehaviour
    {
        [SerializeField] private Animator _animator;

        private const string DirectionInt = "Direction";
        private const string BlockBool = "HasBlock";

        public void Move(Vector3 direction)
        {
            var directionValue = (int)Mathf.Sign(direction.z);
            _animator.SetInteger(DirectionInt, directionValue);
        }

        public void Stay()
        {
            _animator.SetInteger(DirectionInt, 0);
        }

        public void Blocking(bool state)
        {
            _animator.SetBool(BlockBool, state);
        }
    }
}