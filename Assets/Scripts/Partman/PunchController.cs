﻿using Punching;
using UnityEngine;

namespace Partman
{
    public class PunchController : AttackController
    {
        [SerializeField] private float _curveRatio = 0.5f;
        [SerializeField] private FistMover _leftFist;
        [SerializeField] private FistMover _rightFist;

        private TargetQueue _enemyTargets;
        private bool _leftPunch;
        private float _screenHalfWidth;

        private void OnEnable()
        {
            _leftFist.Punching += OnPunching;
            _leftFist.Punched += OnPunched;
            _rightFist.Punching += OnPunching;
            _rightFist.Punched += OnPunched;
        }

        private void OnDisable()
        {
            _leftFist.Punching -= OnPunching;
            _leftFist.Punched -= OnPunched;
            _rightFist.Punching -= OnPunching;
            _rightFist.Punched -= OnPunched;
        }

        private void Start()
        {
            _leftPunch = Random.value > 0.5f;
            _screenHalfWidth = Screen.width / 2f;
        }

        public override void Init(TargetQueue targets)
        {
            _enemyTargets = targets;
        }

        public override void Attack(AttackData data)
        {
            float touchX;

            if (data != null)
            {
                touchX = (data as PartmanAttackData).X;
            }
            else
            {
                touchX = Random.Range(0, _screenHalfWidth * 2f);
            }

            var power = (touchX - _screenHalfWidth) / _screenHalfWidth * _curveRatio;

            if (power == 0)
            {
                if (_leftPunch)
                {
                    _leftFist.PunchTo(_enemyTargets.CurrentTarget.position, power);
                    _leftPunch = false;
                }
                else
                {
                    _rightFist.PunchTo(_enemyTargets.CurrentTarget.position, power);
                    _leftPunch = true;
                }
            }
            else
            {
                if (power > 0)
                {
                    if (_rightFist.IsWaiting)
                    {
                        _rightFist.PunchTo(_enemyTargets.CurrentTarget.position, power);
                    }
                    else if (_leftFist.IsWaiting)
                    {
                        _leftFist.PunchTo(_enemyTargets.CurrentTarget.position, -power);
                    }
                }
                else
                {
                    if (_leftFist.IsWaiting)
                    {
                        _leftFist.PunchTo(_enemyTargets.CurrentTarget.position, power);
                    }
                    else if (_rightFist.IsWaiting)
                    {
                        _rightFist.PunchTo(_enemyTargets.CurrentTarget.position, -power);
                    }
                }
            }
        }

        private void OnPunching()
        {
            AttackingInvoke();
        }

        private void OnPunched()
        {
            AttackedInvoke();
        }
    }
}
