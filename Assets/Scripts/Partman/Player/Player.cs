﻿using PlateManaging;
using Punching;
using UI;
using UnityEngine;

namespace Partman.Player
{
    [RequireComponent(typeof(SwipeController))]
    public class Player : MonoBehaviour
    {
        [SerializeField] private byte _baseMaxPriority = 5;

        private Menu _menu;
        private TargetQueue _targets;
        private SwipeController _swipeController;
        private Transform _camera;
        private Fist[] _fists;

        private const string PlayerStrength = "PlayerStrength";
        private const string PlayerHealth = "PlayerHealth";

        private void Start()
        {
            _swipeController = GetComponent<SwipeController>();
            _fists = GetComponentsInChildren<Fist>();

            _targets = GetComponent<TargetQueue>();
            _targets.Destroyed += OnTargetsDestroyed;
        }

        public void Init(TargetQueue enemy, CameraReplacer camera, Menu menu)
        {
            camera.Init(transform);
            enemy.Destroyed += OnEnemyDestroyed;

            _menu = menu;
            _menu.GameStarted += OnGameStarted;

            _camera = camera.transform;

            GetComponent<PunchController>().Init(enemy);
            GetComponent<MoveController>().Init(enemy.transform);
        }

        public void EnableSwiping()
        {
            GetComponent<SwipeController>().enabled = true;
        }

        private void SetDamage()
        {
            int damage = CalculateValue(PlayerStrength);

            foreach (var fist in _fists)
            {
                fist.SetDamage(damage);
            }
        }

        private void SetPriorities()
        {
            var maxPriority = _baseMaxPriority + CalculateValue(PlayerHealth) - 1;
            var targets = _targets.Targets;

            maxPriority = (int)Mathf.Clamp(maxPriority, 0f, targets.Count);
            targets[targets.Count - 1].SetPriority((byte)maxPriority);

            for (int i = targets.Count - 2; i > -1; i--)
            {
                if (targets[i].Priority >= targets[i + 1].Priority - 1)
                {
                    break;
                }

                var newPriority = targets[i + 1].Priority;
                newPriority--;

                targets[i].SetPriority(newPriority);
            }
        }

        private int CalculateValue(string parameter)
        {
            var value = PlayerPrefs.HasKey(parameter) ? PlayerPrefs.GetInt(parameter) : 1;
            return 1 + (value - 1) / 3;
        }

        private void OnGameStarted()
        {
            _menu.GameStarted -= OnGameStarted;

            SetDamage();
            SetPriorities();
        }

        private void OnTargetsDestroyed()
        {
            _targets.Destroyed -= OnTargetsDestroyed;
            _camera.SetParent(null);

            _swipeController.enabled = false;

            foreach (var fist in _fists)
            {
                fist.Fall(transform.parent);
            }

            foreach (var target in _targets.Untargeted)
            {
                target.FullDestroy();
            }

            gameObject.SetActive(false);
        }

        private void OnEnemyDestroyed()
        {
            _swipeController.enabled = false;
            GetComponent<Collider>().enabled = false;
        }
    }
}