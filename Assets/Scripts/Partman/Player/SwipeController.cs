﻿using UI;
using UnityEngine;

namespace Partman.Player
{
    [RequireComponent(typeof(PunchController), typeof(MoveController))]
    public class SwipeController : MonoBehaviour
    {
        [SerializeField] private PunchController _punchController;
        [SerializeField] private MoveController _moveController;
        [SerializeField] private PartmanAnimator _animator;

        private Vector2 _touchPoint;
        private PunchZone _punchZone;
        private MovingZone _movingZone;
        private JoystickHandler _joystick;

        private void OnEnable()
        {
            var uiHandler = UIHandler.Instance;

            _punchZone = uiHandler.PunchZone;
            _movingZone = uiHandler.MovingZone;

            _punchZone.Punching += OnPunching;
            _movingZone.MoveStarted += OnMoveStarted;
            _movingZone.Moving += OnMoving;
            _movingZone.Moved += OnMoved;
        }

        private void Start()
        {
            _joystick = FindObjectOfType<JoystickHandler>();
        }

        private void OnDisable()
        {
            _punchZone.Punching -= OnPunching;
            _movingZone.MoveStarted -= OnMoveStarted;
            _movingZone.Moving -= OnMoving;
            _movingZone.Moved -= OnMoved;
        }
        
        private void OnPunching(float x)
        {
            var data = new PartmanAttackData(x);
            _punchController.Attack(data);
        }

        private void OnMoveStarted(Vector2 touchPoint)
        {
            _touchPoint = touchPoint;
            _joystick.Show(touchPoint);
        }

        private void OnMoving(Vector2 dragPoint)
        {
            var direction = (dragPoint - _touchPoint).normalized;
            Vector3 moveDirection = new Vector3(direction.x, 0f, direction.y);

            _joystick.OnDrag(dragPoint);
            _moveController.Move(moveDirection);
            _animator.Move(moveDirection);
        }

        private void OnMoved()
        {
            _joystick.Hide();
            _animator.Stay();
            _moveController.Moved();
        }
    }
}
