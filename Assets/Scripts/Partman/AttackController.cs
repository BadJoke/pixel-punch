﻿using UnityEngine;
using UnityEngine.Events;

namespace Partman
{
    public abstract class AttackController : MonoBehaviour
    {
        public event UnityAction Attacking;
        public event UnityAction Attacked;

        public abstract void Attack(AttackData data);

        public abstract void Init(TargetQueue targetQueue);

        protected void AttackingInvoke()
        {
            Attacking?.Invoke();
        }

        protected void AttackedInvoke()
        {
            Attacked?.Invoke();
        }
    }
}