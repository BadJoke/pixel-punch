﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Partman.Blocking
{
    [Serializable]
    public class BlockGroup
    {
        [SerializeField] private float _colliderYPosition;
        [SerializeField] private List<BlockHealth> _blocks;

        public event Action<float> Destroyed;

        public void Init()
        {
            foreach (var block in _blocks)
            {
                block.Destroyed += OnBlockDestroyed;
            }
        }

        private void OnBlockDestroyed(BlockHealth block)
        {
            block.Destroyed -= OnBlockDestroyed;
            _blocks.Remove(block);

            if (_blocks.Count == 0)
            {
                Destroyed?.Invoke(_colliderYPosition);
            }
        }
    }
}