﻿using System.Collections.Generic;
using UnityEngine;

namespace Partman.Blocking
{
    public class BlockDestroyingActivator : MonoBehaviour
    {
        [SerializeField, Range(0.01f, 1f)] private float _destroyPercent;
        [SerializeField] private Collider[] _disabledColliders;
        [SerializeField] private List<BlockHealth> _destroyableBlocks;

        private int _enablingCount;

        private void OnEnable()
        {
            foreach (var block in _destroyableBlocks)
            {
                block.Destroyed += OnBlockDestroyed;
            }
        }

        private void Start()
        {
            _enablingCount = (int)(_destroyableBlocks.Count * (1f - _destroyPercent));
        }

        private void OnDisable()
        {
            foreach (var block in _destroyableBlocks)
            {
                block.Destroyed -= OnBlockDestroyed;
            }
        }

        private void OnBlockDestroyed(BlockHealth block)
        {
            block.Destroyed -= OnBlockDestroyed;

            _destroyableBlocks.Remove(block);

            if (_destroyableBlocks.Count <= _enablingCount)
            {
                foreach (var collider in _disabledColliders)
                {
                    collider.enabled = true;
                }

                enabled = false;
            }
        }
    }
}
