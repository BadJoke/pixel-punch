﻿using System;
using UnityEngine;

namespace Partman.Blocking
{
    public class BlockHealth : MonoBehaviour
    {
        [SerializeField] private int _health = 2;
        [SerializeField] private byte _targetPriority;

        public event Action Damaged;
        public event Action<BlockHealth> Destroyed;
        public event Action<BlockHealth> FullDestroyed;

        public byte Priority => _targetPriority;

        public void SetHealth(int value)
        {
            _health = value;
        }

        public void SetPriority(byte value)
        {
            _targetPriority = value;
        }

        public void TakeDamage(int damage)
        {
            _health -= damage;

            if (_health > 0)
            {
                Damaged?.Invoke();
                return;
            }

            Destroy();
        }

        public void FullDestroy()
        {
            _health = 0;

            FullDestroyed?.Invoke(this);
        }
        
        private void Destroy()
        {
            Destroyed?.Invoke(this);
        }
    }
}
