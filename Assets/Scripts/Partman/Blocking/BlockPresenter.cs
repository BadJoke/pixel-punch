﻿using UnityEngine;

namespace Partman.Blocking
{
    [RequireComponent(typeof(BlockHealth))]
    public class BlockPresenter : MonoBehaviour
    {
        [SerializeField] private ParticleSystem _crumblingEffect;
        [SerializeField] private ParticleSystem _smallCrumblingEffect;

        private BlockHealth _block;
        private Color _originalColor;
        private Color _originalColorEmission;

        private const float ParticleSpeedModifier = 5f;
        private const string ColorName = "_Color";
        private const string EmissionName = "_Emission";

        private void OnEnable()
        {
            _block = GetComponent<BlockHealth>();

            _block.Damaged += OnBlockDamaged;
            _block.Destroyed += OnBlockDestroyed;
            _block.FullDestroyed += OnBlockFullDestroyed;
        }

        private void Start()
        {
            SetColors();
        }

        private void OnDisable()
        {
            _block.Damaged -= OnBlockDamaged;
            _block.Destroyed -= OnBlockDestroyed;
            _block.FullDestroyed -= OnBlockDestroyed;
        }

        private void SetColors()
        {
            var material = GetComponent<MeshRenderer>().material;
            _originalColor = material.GetColor(ColorName);
            _originalColorEmission = material.GetColor(EmissionName);
        }

        private void Destroy(float particleSpeedModifier)
        {
            var effect = Instantiate(_crumblingEffect);
            effect.transform.position = transform.position;

            var renderer = effect.GetComponent<ParticleSystemRenderer>();
            renderer.material.SetColor(ColorName, _originalColor);
            renderer.material.SetColor(EmissionName, _originalColorEmission);

            effect.startSpeed *= particleSpeedModifier;

            effect.Play();

            gameObject.SetActive(false);
        }

        private void OnBlockDamaged()
        {
            var effect = Instantiate(_smallCrumblingEffect);
            effect.transform.position = transform.position;

            var renderer = effect.GetComponent<ParticleSystemRenderer>();
            renderer.material.SetColor(ColorName, _originalColor);
            renderer.material.SetColor(EmissionName, _originalColorEmission);

            effect.Play();
        }

        private void OnBlockDestroyed(BlockHealth block)
        {
            Destroy(1f);
        }

        private void OnBlockFullDestroyed(BlockHealth block)
        {
            Destroy(ParticleSpeedModifier);
        }
    }
}
