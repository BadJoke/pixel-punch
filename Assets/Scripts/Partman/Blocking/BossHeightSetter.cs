﻿using DG.Tweening;
using UnityEngine;

namespace Partman.Blocking
{
    public class BossHeightSetter : MonoBehaviour
    {
        [SerializeField] private BlockGroup _footGroup;
        [SerializeField] private BlockGroup _kneeGroup;
        [SerializeField] private BlockGroup _upLegGroup;
        [SerializeField] private BlockGroup _hipsGroup;

        private void Start()
        {
            InitGroups();
        }

        private void InitGroups()
        {
            _footGroup.Init();
            _kneeGroup.Init();
            _upLegGroup.Init();
            _hipsGroup.Init();

            _footGroup.Destroyed += OnGroupDestroyed;
            _kneeGroup.Destroyed += OnGroupDestroyed;
            _upLegGroup.Destroyed += OnGroupDestroyed;
            _hipsGroup.Destroyed += OnGroupDestroyed;
        }

        private void OnGroupDestroyed(float yPosition)
        {
            yPosition *= transform.localScale.y / 2f;

            if (transform.localPosition.y < yPosition)
            {
                return;
            }

            var newPosition = Vector3.zero;
            newPosition.y = yPosition;

            transform.DOLocalMove(newPosition, 0.5f);
        }
    }
}