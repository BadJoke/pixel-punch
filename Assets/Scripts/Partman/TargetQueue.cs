﻿using Partman.Blocking;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace Partman
{
    public class TargetQueue : MonoBehaviour
    {
        private List<BlockHealth> _targets;
        private BlockHealth _currentTarget;
        private BlockHealth[] _untargeted;
        private float _targetSwitchingDelay = 0.1f;

        public event UnityAction Destroyed;

        public Transform CurrentTarget => _currentTarget.transform;
        public BlockHealth[] Untargeted => _untargeted;
        public List<BlockHealth> Targets => _targets;

        private void Awake()
        {
            _targets = GetComponentsInChildren<BlockHealth>().ToList();

            SortTargets();

            foreach (var target in _targets)
            {
                target.Destroyed += OnTargetDestroyed;
            }

            SetCurrentTarget();
        }

        private void OnDestroy()
        {
            foreach (var target in _targets)
            {
                target.Destroyed -= OnTargetDestroyed;
            }
        }

        private void SortTargets()
        {
            _untargeted = _targets.Where(target => target.Priority == 0).ToArray();

            foreach (var untarget in _untargeted)
            {
                _targets.Remove(untarget);
            }

            _targets = _targets.OrderBy(target => target.Priority).ToList();

            var maxPriority = _targets[_targets.Count - 1].Priority;

            for (int i = 0; i < maxPriority; i++)
            {
                var index = _targets.FindIndex(target => target.Priority == i);
                var localTargets = _targets.Where(target => target.Priority == i).ToList();

                if (localTargets.Count() < 2)
                {
                    continue;
                }

                localTargets = Shuffle(localTargets, true);

                for (int j = 0; j < localTargets.Count(); j++)
                {
                    _targets[index + j] = localTargets[j];
                }
            }
        }

        private List<T> Shuffle<T>(List<T> list, bool trueRand)
        {
            var count = list.Count;
            var last = count - 1;
            var random = new System.Random();

            for (var i = 0; i < last; i++)
            {
                var randomIndex = trueRand ? random.Next(i, count) : Random.Range(i, count);
                var tmp = list[i];
                list[i] = list[randomIndex];
                list[randomIndex] = tmp;
            }

            return list;
        }

        private void SetCurrentTarget()
        {
            if (_targets.Count > 0)
            {
                _currentTarget = _targets[0];
            }
            else
            {
                _currentTarget = null;
            }
        }

        private void Destroy()
        {
            Destroyed?.Invoke();
        }

        private void OnTargetDestroyed(BlockHealth target)
        {
            target.Destroyed -= OnTargetDestroyed;
            _targets.Remove(target);

            if (_targets.Count > 0)
            {
                Invoke(nameof(SetCurrentTarget), _targetSwitchingDelay);
            }
            else
            {
                Destroy();
            }
        }
    }
}
