﻿using Partman.Blocking;
using Partman.Enemy;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Partman
{
    public class DamageTaker : MonoBehaviour
    {
        [SerializeField] private TargetQueue _targetQueue;
        [SerializeField] private BodyKickbaker _kickbaker;

        private CameraShaker _cameraShaker;
        private Transform _damagedFist;
        private List<BlockHealth> _blocks;

        private void Start()
        {
            _blocks = _targetQueue.Targets;

            foreach (var block in _blocks)
            {
                block.Destroyed += OnBlockDestroyed;
            }
        }

        private void OnDestroy()
        {
            foreach (var block in _blocks)
            {
                block.Destroyed -= OnBlockDestroyed;
            }
        }

        public void Init(CameraShaker shaker)
        {
            _cameraShaker = shaker;
        }

        public void Take(int damage, Transform fist)
        {
            _damagedFist = fist;

            _cameraShaker?.Shake();
            _kickbaker?.Kickbacke(_damagedFist);

            var blocks = _blocks.Where((block) => block.Priority == _blocks[0].Priority).ToArray();
            var additionalCount = (int)Mathf.Clamp(damage - 1, 0f, _blocks.Count - blocks.Length);
            var additionalBlocks = _blocks.GetRange(blocks.Length, additionalCount);

            foreach (var block in blocks)
            {
                block.TakeDamage(damage);
            }

            foreach (var block in additionalBlocks)
            {
                block.TakeDamage(damage);
            }
        }

        private void OnBlockDestroyed(BlockHealth destroyedBlock)
        {
            destroyedBlock.Destroyed -= OnBlockDestroyed;

            _blocks.Remove(destroyedBlock);
        }
    }
}
