﻿namespace Partman
{
    public class PartmanAttackData : AttackData
    {
        public float X { get; private set; }

        public PartmanAttackData(float x)
        {
            X = x;
        }
    }
}
