﻿using System.Collections;
using UnityEngine;

namespace Partman.Enemy
{
    public class BodyKickbaker : MonoBehaviour
    {
        [SerializeField] private Transform _bone;
        [SerializeField] private float _returnSpeed;
        [SerializeField] private float _kickbackingDepth;

        private Quaternion _startRotation;
        private Quaternion _kickbackingTilt = new Quaternion();
        private Coroutine _returningToDefault;

        private void Start()
        {
            _startRotation = _bone.transform.localRotation;
        }

        private void LateUpdate()
        {
            if (_returningToDefault == null)
            {
                return;
            }

            var tilt = Quaternion.Euler(_kickbackingTilt.eulerAngles);
            _bone.localRotation = tilt;
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        public void Kickbacke(Transform fist)
        {
            if (_returningToDefault == null)
            {
                _returningToDefault = StartCoroutine(Kickbaking(fist));
            }
            else
            {
                StopCoroutine(_returningToDefault);
                _returningToDefault = StartCoroutine(Kickbaking(fist));
            }
        }

        private IEnumerator Kickbaking(Transform fist)
        {
            var myPosition = transform.position;
            myPosition.y = 0f;

            var fistPosition = fist.position;
            fistPosition.y = 0f;

            var direction = (myPosition - fistPosition).normalized;
            direction = _bone.InverseTransformDirection(direction);

            var tiltEulers = new Vector3(direction.z, 0f, -direction.x) * _kickbackingDepth;
            Quaternion tilt = Quaternion.Euler(tiltEulers);
            
            for (float step = 0f; step <= 1f; step += _returnSpeed * Time.deltaTime)
            {
                var sinValue = Mathf.Sin(step * Mathf.PI);
                _kickbackingTilt = Quaternion.Lerp(_startRotation, tilt, sinValue);
                
                yield return null;
            }

            _returningToDefault = null;
        }
    }
}