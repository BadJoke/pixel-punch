﻿using UnityEngine;

namespace Partman.Enemy
{
    public class BlockController : MonoBehaviour
    {
        [SerializeField] private float _duration;
        [SerializeField] private float _durationRange;
        [SerializeField] private float _interval;
        [SerializeField] private float _intervalRange;
        [SerializeField] private Collider _collider;
        [SerializeField] private Block _block;

        private float _minDuration;
        private float _maxDuration;
        private float _minInterval;
        private float _maxInterval;

        private void Start()
        {
            _minInterval = _interval - _intervalRange;
            _maxInterval = _interval + _intervalRange;
            _minDuration = _duration - _durationRange;
            _maxDuration = _duration + _durationRange;
        }

        public float GetInterval()
        {
            return Random.Range(_minInterval, _maxInterval);
        }

        public float GetDuration()
        {
            return Random.Range(_minDuration, _maxDuration);
        }

        public void SetActive(bool active)
        {
            _block.SetActive(active);

            _collider.enabled = active;
        }
    }
}
