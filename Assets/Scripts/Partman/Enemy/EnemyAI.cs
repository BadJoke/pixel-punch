﻿using Punching;
using System.Collections;
using UnityEngine;

namespace Partman.Enemy
{
    [RequireComponent(typeof(MoveController))]
    public class EnemyAI : MonoBehaviour
    {
        [SerializeField] private AttackController _attackController;
        [SerializeField] private MoveController _moveController;
        [SerializeField] private BlockController _blockController;
        [SerializeField] private PartmanAnimator _animator;
        [SerializeField] private float _stayingTime = 2f;
        [SerializeField] private float _movingTime = 1.5f;
        [SerializeField] private float _stayingProbability = 0.1f;
        [SerializeField] private float _movingProbability = 0.4f;
        [SerializeField] private float _attackingProbability = 1f;

        private bool _isFirstChoosing = true;
        private bool _isMoving = false;
        private bool _isPunching = false;
        private bool _isBlocking = false;
        private float _time;
        private float _stayingProbabilityLimit;
        private float _movingProbabilityLimit;
        private float _attckingProbabilityLmit;
        private Vector3 _direction;
        private TargetQueue _myTargets;
        private TargetQueue _playerTargets;
        private Fist[] _fists;

        private void OnEnable()
        {
            _myTargets = GetComponentInChildren<TargetQueue>();
            _myTargets.Destroyed += OnMyTargetsDestroyed;

            _attackController.Attacking += OnAttacking;
            _attackController.Attacked += OnAttacked;
        }

        private void Start()
        {
            _stayingProbabilityLimit = _stayingProbability;
            _movingProbabilityLimit = _stayingProbabilityLimit + _movingProbability;
            _attckingProbabilityLmit = _movingProbabilityLimit + _attackingProbability;
            _fists = GetComponentsInChildren<Fist>();

            ChangeBehaviour();

            if (_blockController)
            {
                StartCoroutine(Blocking());
            }
        }

        private void Update()
        {
            if (_isMoving)
            {
                _time += Time.deltaTime;
                _moveController.Move(_direction);

                if (_time >= _movingTime)
                {
                    StopMoving();
                    ChangeBehaviour();
                }
            }
        }

        private void OnDisable()
        {
            _attackController.Attacking -= OnAttacking;
            _attackController.Attacked -= OnAttacked;

            CancelInvoke();
            StopAllCoroutines();
        }

        public void Init(TargetQueue targets)
        {
            _playerTargets = targets;

            _playerTargets.Destroyed += OnPlayerTargtesDestroyed;
        }

        private void ChangeBehaviour()
        {
            float probability;

            if (_isFirstChoosing)
            {
                do
                {
                    probability = Random.value * _attckingProbabilityLmit;
                }
                while (probability >= _movingProbabilityLimit);

                _isFirstChoosing = false;
            }
            else
            {
                probability = Random.value * _attckingProbabilityLmit;
            }

            if (probability < _stayingProbabilityLimit)
            {
                Stay();
            }
            else if (probability < _movingProbabilityLimit)
            {
                StartMoving();
            }
            else
            {
                Invoke(nameof(Attack), 0.11f);
            }
        }

        private void Attack()
        {
            StartCoroutine(Attacking());
        }

        private IEnumerator Attacking()
        {
            yield return new WaitWhile(() => _isBlocking);

            _attackController.Attack(null);
        }

        private void StartMoving()
        {
            _time = 0;
            _isMoving = true;
            _direction = Random.insideUnitSphere;
            _direction.y = 0;
            _animator?.Move(_direction);
        }

        private void StopMoving()
        {
            _isMoving = false;
            _moveController.Moved();
            _animator?.Stay();
        }

        private void Stay()
        {
            Invoke(nameof(ChangeBehaviour), _stayingTime);
        }

        private IEnumerator Blocking()
        {
            yield return new WaitForSeconds(_blockController.GetInterval());

            while (true)
            {
                yield return new WaitWhile(() => _isPunching);

                SetBlockState(true);

                yield return new WaitForSeconds(_blockController.GetDuration());

                SetBlockState(false);

                yield return new WaitForSeconds(_blockController.GetInterval());
            }
        }

        private void SetBlockState(bool state)
        {
            _isBlocking = state;

            _blockController.SetActive(state);
            _animator.Blocking(state);
        }

        private void OnAttacking()
        {
            _isPunching = true;
            _moveController.enabled = false;
        }

        private void OnAttacked()
        {
            _isPunching = false;
            _moveController.enabled = true;

            ChangeBehaviour();
        }

        private void OnMyTargetsDestroyed()
        {
            foreach (var glow in _fists)
            {
                glow.Fall(transform.parent);
            }

            foreach (var target in _myTargets.Untargeted)
            {
                target.FullDestroy();
            }

            gameObject.SetActive(false);
        }

        private void OnPlayerTargtesDestroyed()
        {
            _playerTargets.Destroyed -= OnPlayerTargtesDestroyed;

            enabled = false;
        }
    }
}
