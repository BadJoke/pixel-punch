﻿using Partman.Blocking;
using Partman.Enemy.Boss.Zones;
using System.Collections;
using UnityEngine;

namespace Partman.Enemy.Boss.EyeOfCtulhu
{
    public class CtulhuEyeAttackController : AttackController
    {
        [SerializeField] private float _radius = 3f;
        [SerializeField] private Transform _zoneContainer;
        [SerializeField] private CircleZone _zone;
        [SerializeField] private Transform _spitPoint;
        [SerializeField] private Spit _spitPrefab;
        [SerializeField] private ParticleSystem _charge;
        [SerializeField] private BlockGroup _attackGroup;

        private Transform _target;

        private void Start()
        {
            _attackGroup.Destroyed += OnAttackGroupDestroyed;
            _attackGroup.Init();
        }

        public override void Init(TargetQueue targetQueue)
        {
            _target = targetQueue.transform;
        }

        public override void Attack(AttackData data)
        {
            _charge.Play();

            AttackingInvoke();
        }

        private void Atacking()
        {
            _charge.Stop();
            _charge.Clear();

            StartCoroutine(Spitting());
        }

        private IEnumerator Spitting()
        {
            var zone = Instantiate(_zone, _zoneContainer);
            zone.SetZonePosition(_target.position);
            zone.Show();

            Spit(zone);

            var rightZone = Instantiate(_zone, _zoneContainer);
            rightZone.SetZonePosition(_target.position + zone.transform.right * _radius);
            rightZone.Show();

            Spit(rightZone);

            var leftZone = Instantiate(_zone, _zoneContainer);
            leftZone.SetZonePosition(_target.position - zone.transform.right * _radius);
            leftZone.Show();

            Spit(leftZone);

            yield return new WaitForSeconds(0.75f);

            AttackedInvoke();
        }

        private void Spit(CircleZone zone)
        {
            zone.transform.SetParent(null);

            Instantiate(_spitPrefab).Init(zone, _spitPoint.position);
        }

        private void OnAttackGroupDestroyed(float y)
        {
            _attackGroup.Destroyed -= OnAttackGroupDestroyed;

            StopAllCoroutines();
        }
    }
}
