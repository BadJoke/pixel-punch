﻿using UnityEngine;

namespace Partman.Enemy.Boss.Zones
{
    public class CircleZone : Zone
    {
        [SerializeField] private Transform _target;

        public Vector3 Position => _target.position;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(Sprite.transform.position, Size / 2);
        }

        public void SetZonePosition(Vector3 position)
        {
            Sprite.transform.position = position;
        }
    }
}
