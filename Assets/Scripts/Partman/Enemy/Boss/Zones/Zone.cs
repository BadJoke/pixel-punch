﻿using DG.Tweening;
using UnityEngine;

namespace Partman.Enemy.Boss.Zones
{
    public abstract class Zone : MonoBehaviour
    {
        [SerializeField] protected float Size = 1f;
        [SerializeField] protected SpriteRenderer Sprite;

        public float ZoneSize => Size;

        public void Hide()
        {
            Sprite.DOFade(0f, 0.33f).onComplete += () =>
            {
                Destroy(gameObject);
            };
        }

        public void Show()
        {
            Sprite.DOFade(1f, 0.33f);
        }
    }
}
