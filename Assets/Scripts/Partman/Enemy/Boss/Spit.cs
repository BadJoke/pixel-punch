﻿using Partman.Enemy.Boss.Zones;
using UnityEngine;

namespace Partman.Enemy.Boss
{
    public class Spit : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private LayerMask _destroyLayers;
        [SerializeField] private ParticleSystem _crumbling;

        private CircleZone _zone;
        private Vector3 _moveDirection;

        private void FixedUpdate()
        {
            _rigidbody.MovePosition(transform.position + _moveDirection * _speed * Time.deltaTime);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (_destroyLayers == (_destroyLayers | (1 << other.gameObject.layer)))
            {
                CancelInvoke();

                _zone.Hide();
                _crumbling.transform.parent = null;
                _crumbling.Play();

                var collisions = GetColliders();

                foreach (var collision in collisions)
                {
                    if (collision.TryGetComponent(out DamageTaker player))
                    {
                        player.Take(1, transform);
                    }
                }

                Destroy(gameObject);
            }
        }

        public void Init(CircleZone zone, Vector3 startPosition)
        {
            Invoke(nameof(Destroy), 6f);

            transform.position = startPosition;

            _moveDirection = (zone.Position - startPosition).normalized;
            _zone = zone;
            _crumbling.transform.parent = transform;
            _crumbling.transform.localPosition = Vector3.zero;
        }

        private Collider[] GetColliders()
        {
            return Physics.OverlapSphere(transform.position, _zone.ZoneSize * 2f, _destroyLayers);
        }

        private void Destroy()
        {
            _zone.Hide();

            Destroy(gameObject);
        }
    }
}