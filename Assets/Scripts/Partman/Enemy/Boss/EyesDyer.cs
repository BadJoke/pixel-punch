﻿using DG.Tweening;
using UnityEngine;

namespace Partman.Enemy.Boss
{
    public class EyesDyer : MonoBehaviour
    {
        [SerializeField] private MeshRenderer[] _eyes;
        [SerializeField] private AttackController _attackController;
        [SerializeField] private Color _eyesAttackColor = Color.red;
        [SerializeField] private Color _eyesAttackColorEmission = Color.red;

        private Color _originalColor;
        private Color _originalColorEmission;

        private const string ColorName = "_Color";
        private const string EmissionName = "_Emission";

        private void OnEnable()
        {
            _attackController.Attacking += OnAttacking;
            _attackController.Attacked += OnAttacked;
        }

        private void Start()
        {
            var material = new Material(_eyes[0].material);
            _originalColor = material.GetColor(ColorName);
            _originalColorEmission = material.GetColor(EmissionName);

            foreach (var eye in _eyes)
            {
                eye.material = material;
            }
        }

        private void OnDisable()
        {
            _attackController.Attacking -= OnAttacking;
            _attackController.Attacked -= OnAttacked;
        }

        private void OnAttacking()
        {
            foreach (var eye in _eyes)
            {
                eye.material.DOColor(_eyesAttackColor, ColorName, 0.5f);
                eye.material.DOColor(_eyesAttackColorEmission, EmissionName, 0.5f);
            }
        }

        private void OnAttacked()
        {
            foreach (var eye in _eyes)
            {
                eye.material.DOColor(_originalColor, ColorName, 0.5f);
                eye.material.DOColor(_originalColorEmission, EmissionName, 0.5f);
            }
        }
    }

}
