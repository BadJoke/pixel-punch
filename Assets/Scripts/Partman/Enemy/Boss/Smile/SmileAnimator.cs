﻿using UnityEngine;

namespace Partman.Enemy.Boss.Smile
{
    public class SmileAnimator : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private AttackController _attackController;

        private void OnEnable()
        {
            _attackController.Attacking += OnAttacking;
            _attackController.Attacked += OnAttacked;
        }

        private void OnDisable()
        {
            _attackController.Attacking -= OnAttacking;
            _attackController.Attacked -= OnAttacked;
        }

        private void OnAttacking()
        {
            _animator.SetTrigger("Attack");
        }

        private void OnAttacked()
        {
            _animator.SetTrigger("Attacked");
        }
    }

}
