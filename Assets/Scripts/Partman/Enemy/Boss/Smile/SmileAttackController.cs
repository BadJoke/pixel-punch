﻿using Partman.Blocking;
using Partman.Enemy.Boss.Zones;
using System.Collections;
using UnityEngine;

namespace Partman.Enemy.Boss.Smile
{
    public class SmileAttackController : AttackController
    {
        [SerializeField] private byte _spitCount = 3;
        [SerializeField] private float _spitIntreval = 0.15f;
        [SerializeField] private float _replaceZoneDelay = 1f;
        [SerializeField] private Transform _zoneContainer;
        [SerializeField] private CircleZone _zone;
        [SerializeField] private Transform _spitPoint;
        [SerializeField] private Spit _spitPrefab;
        [SerializeField] private ParticleSystem _charge;
        [SerializeField] private BlockGroup _attackGroup;

        private Transform _target;

        private void OnEnable()
        {
            _attackGroup.Destroyed += OnAttackGroupDestroyed;
        }

        private void Start()
        {
            _attackGroup.Init();
        }

        public override void Init(TargetQueue targetQueue)
        {
            _target = targetQueue.transform;
        }

        public override void Attack(AttackData data)
        {
            _charge.Play();

            AttackingInvoke();
        }

        private void Atacking()
        {
            _charge.Stop();
            _charge.Clear();

            StartCoroutine(Spitting());
        }

        private IEnumerator Spitting()
        {
            for (int i = 0; i < _spitCount; i++)
            {
                var zone = Instantiate(_zone, _zoneContainer);
                zone.SetZonePosition(_target.position);
                zone.Show();

                yield return new WaitForSeconds(_spitIntreval);

                Spit(zone);
            }

            yield return new WaitForSeconds(_replaceZoneDelay);

            AttackedInvoke();
        }

        private void Spit(CircleZone zone)
        {
            zone.transform.SetParent(null);

            Instantiate(_spitPrefab).Init(zone, _spitPoint.position);
        }

        private void OnAttackGroupDestroyed(float y)
        {
            _attackGroup.Destroyed -= OnAttackGroupDestroyed;

            StopAllCoroutines();
        }
    }
}
