﻿using System;
using UnityEngine;

namespace Partman.Enemy.Boss
{
    public class PlayerObserver : MonoBehaviour
    {
        [SerializeField] private Transform _player;
        [SerializeField, Range(0f, 5f)] private float _folowingSpeed = 0.5f;

        private bool _hasObserving = true;

        private const float RotationAngle = 0.03f;

        public event Action<float> Moving;
        public event Action Staying;

        private void Update()
        {
            if (_hasObserving)
            {
                CheckAngle();
                LookAtPlayer();
            }
        }

        public void PauseObserving()
        {
            _hasObserving = false;
            Staying?.Invoke();
        }

        public void ResumeObserving()
        {
            _hasObserving = true;
        }

        private void LookAtPlayer()
        {
            var rotation = transform.rotation;
            transform.LookAt(_player);

            var rot = transform.rotation.eulerAngles;
            rot.x = 0;
            
            transform.rotation = Quaternion.Euler(rot);
            transform.rotation = Quaternion.Slerp(rotation, transform.rotation, Time.deltaTime * _folowingSpeed);
        }

        private void CheckAngle()
        {
            var angle = GetAngle();
            if (Math.Abs(angle) > RotationAngle)
            {
                Moving?.Invoke(angle);
            }
            else
            {
                Staying?.Invoke();
            }
        }

        private float GetAngle()
        {
            var direction = (_player.position - transform.position).normalized;
            return Vector3.Cross(direction, transform.forward).y;
        }
    }

}
