﻿using Partman.Blocking;
using Partman.Enemy.Boss.Zones;
using System.Collections;
using UnityEngine;

namespace Partman.Enemy.Boss.SauronTower
{
   

    public class TowerAttackController : AttackController
    {
        [SerializeField] private byte _spitCount = 3;
        [SerializeField] private float _spitIntreval = 0.15f;
        [SerializeField] private Transform _zoneContainer;
        [SerializeField] private CircleZone _zone;
        [SerializeField] private Transform _spitPoint;
        [SerializeField] private Spit _spitPrefab;
        [SerializeField] private ParticleSystem _charge;
        [SerializeField] private ChargeListener _chargeListener;
        [SerializeField] private BlockGroup _attackGroup;

        private Transform _target;
        private CircleZone[] _attackZones;
        private Vector3 _startZonePosinion = new Vector3(0f, 0.01f, 0f);

        private void OnEnable()
        {
            _chargeListener.Charged += OnCharged;
        }

        private void Start()
        {
            _attackGroup.Destroyed += OnAttackGroupDestroyed;
            _attackGroup.Init();
        }

        private void OnDisable()
        {
            _chargeListener.Charged -= OnCharged;
        }

        public override void Init(TargetQueue targetQueue)
        {
            _target = targetQueue.transform;
        }

        public override void Attack(AttackData data)
        {
            _charge.Play();

            AttackingInvoke();
        }

        private IEnumerator Spitting()
        {
            for (byte i = 0; i < _spitCount; i++)
            {
                var zone = Instantiate(_zone, _zoneContainer);
                zone.SetZonePosition(_target.position);
                zone.Show();

                yield return new WaitForSeconds(_spitIntreval);

                Spit(zone);
            }

            yield return new WaitForSeconds(_spitIntreval * 16f);

            AttackedInvoke();
        }

        private void Spit(CircleZone zone)
        {
            zone.transform.SetParent(null);

            Instantiate(_spitPrefab).Init(zone, _spitPoint.position);
        }

        private void OnCharged()
        {
            StartCoroutine(Spitting());
        }

        private void OnAttackGroupDestroyed(float y)
        {
            _attackGroup.Destroyed -= OnAttackGroupDestroyed;

            StopAllCoroutines();
            enabled = false;
        }
    }
}
