﻿using System;
using UnityEngine;

namespace Partman.Enemy.Boss.SauronTower
{
    public class ChargeListener : MonoBehaviour
    {
        public event Action Charged;

        public void OnParticleSystemStopped()
        {
            Charged?.Invoke();
        }
    }
}
