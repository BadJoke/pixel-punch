﻿using DG.Tweening;
using UnityEngine;

namespace Partman.Enemy
{
    public class Block : MonoBehaviour
    {
        [SerializeField] private ParticleSystem _crumblingEffect;
        [SerializeField] private int _playerLayer;
        [SerializeField] private float _showingTime = 0.5f;

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == _playerLayer)
            {
                _crumblingEffect.Play();
            }    
        }

        public void SetActive(bool active)
        {
            transform.DOScale(active ? 1f : 0f, _showingTime);
        }
    }
}
