﻿using UI;
using UnityEngine;

namespace Partman.Enemy
{
    [RequireComponent(typeof(EnemyAI))]
    public class Enemy : MonoBehaviour
    {
        private Menu _menu;
        private EnemyAI _ai;

        private void Awake()
        {
            _ai = GetComponent<EnemyAI>();
        }

        public void Init(TargetQueue targetQueue, CameraShaker shaker, Menu menu)
        {
            _menu = menu;
            _menu.GameStarted += OnGameStarted;

            _ai.Init(targetQueue);

            GetComponentInChildren<AttackController>().Init(targetQueue);
            GetComponent<MoveController>().Init(targetQueue.transform);
            GetComponentInChildren<DamageTaker>().Init(shaker);
        }

        private void OnGameStarted()
        {
            _menu.GameStarted -= OnGameStarted;

            _ai.enabled = true;
        }
    }
}
