﻿using UnityEngine;
using UnityEngine.AI;

namespace Partman
{
    public class MoveController : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private Transform _lookingTarget;
        [SerializeField] private Transform _body;
        [SerializeField] private Transform _spineBone;

        private bool _isMoving;
        private Vector3 _direction;
        private Rigidbody _rigidbody;
        private NavMeshAgent _navMeshAgent;
        private Vector3 _lookingDirection = Vector3.forward;
        private Vector3 _additionRotation = new Vector3(0f, 20f, 0f);

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        private void Update()
        {
            var rotation = transform.rotation;
            transform.LookAt(_lookingTarget);
            transform.rotation = Quaternion.Lerp(rotation, transform.rotation, 0.05f);

            if (_body)
            {
                var lookRotation = Quaternion.LookRotation(_lookingDirection);
                _body.localRotation = Quaternion.Lerp(_body.localRotation, lookRotation, 0.05f);
            }

            if (_spineBone)
            {
                _spineBone.LookAt(_lookingTarget);
                _spineBone.Rotate(_additionRotation);
            }

            if (_isMoving)
            {
                _navMeshAgent.Move((transform.rotation * _direction) * _speed * Time.deltaTime);
            }
        }

        private void OnCollisionExit(Collision collision)
        {
            _rigidbody.velocity = Vector3.zero;
        }

        private void OnTriggerExit(Collider other)
        {
            _rigidbody.velocity = Vector3.zero;
        }

        public void Init(Transform lookingTarget)
        {
            _lookingTarget = lookingTarget;
        }

        public void Move(Vector3 direction)
        {
            _direction = direction;
            _lookingDirection = direction;

            if (_lookingDirection.z < 0)
            {
                _lookingDirection *= -1f;
            }

            _isMoving = true;
        }

        public void Moved()
        {
            _isMoving = false;
            _lookingDirection = Vector3.forward;
        }
    }
}
