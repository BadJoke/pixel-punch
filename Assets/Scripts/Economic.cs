﻿using UnityEngine;

public static class Economic
{
    private static int _baswReward = 100;
    private static int _baseUpgradePrice = 50;

    public static int GetUpgradePrice(int rate)
    {
        if (rate == 1)
        {
            return _baseUpgradePrice;
        }

        return _baseUpgradePrice + (int)(Mathf.Pow(rate, 2f) * 6f);
    }

    public static int GetReward(bool hasWin, int level)
    {
        if (level == 1)
        {
            return _baswReward;
        }

        var reward = _baswReward + (int)(Mathf.Pow(level, 2f) * 4f);

        if (hasWin == false)
        {
            reward /= 5;
        }

        return reward;
    }
}
