﻿using UnityEngine;
using UnityEngine.Events;

namespace Punching
{
    public class FistMover : MonoBehaviour
    {
        [SerializeField] private float _movingTime;
        [SerializeField] private Vector3 _power;
        [SerializeField] private Transform _parent;
        [SerializeField] private Transform _target;
        [SerializeField] private Fist _fist;
        [SerializeField] private GameObject _fullArm;
        [SerializeField] private ParticleSystem _punchCrumbling;

        private bool _isMoving = false;
        private bool _isMoveBack = false;
        private float _time = 0f;
        private int _crumblingFrame = 0;
        private Vector3 _start;
        private Vector3 _end;
        private Transform _startParent;
        private Quaternion _startRotation;
        private TrailMaker _trail;

        public bool IsWaiting => !_isMoving;

        public event UnityAction Punching;
        public event UnityAction Punched;

        private void OnEnable()
        {
            _fist.Collided += OnFistCollided;
        }

        private void Start()
        {
            _trail = GetComponent<TrailMaker>();
            _startParent = transform.parent;
            _start = transform.localPosition;
            _startRotation = transform.localRotation;
        }

        private void FixedUpdate()
        {
            if (_isMoving)
            {
                Move();
            }
        }

        public void PunchTo(Vector3 targetPosition, float xPower)
        {
            if (_isMoving == false)
            {
                transform.LookAt(targetPosition);
                transform.SetParent(_parent);

                _isMoving = true;
                _isMoveBack = false;
                _target.position = targetPosition;
                _power.x = xPower;
                _end = _target.localPosition;
                _fist.transform.SetParent(transform);
                _fist.Enable();
                _fullArm.SetActive(false);

                //SpawnTrail();
                _trail.Spawn(_start, _end, _power, _movingTime);
                Punching?.Invoke();
            }
        }

        private void Move()
        {
            _time += _isMoveBack ? -Time.fixedDeltaTime : Time.fixedDeltaTime;
            
            //ScrollTrail();

            //_fist.transform.position = _trail[_trailIndex].transform.position;
            _fist.transform.position = _trail.Scroll(_isMoveBack, _time).position;

            if (_isMoveBack == false)
            {
                if (_crumblingFrame % 2 == 0)
                {
                    var effect = Instantiate(_punchCrumbling);
                    effect.transform.position = _fist.transform.position;
                    effect.Play();
                }

                _crumblingFrame++;
            }

            //if (_trailIndex < _trail.Count - 1)
            //{
            //    _fist.transform.LookAt(_trail[_trailIndex + 1].transform);
            //}
            _trail.RotateFist(_fist.transform);

            if (_time >= _movingTime)
            {
                _isMoveBack = true;
            }

            if (_time <= 0f)
            {
                _fist.SetStartParameters();
                _fullArm.SetActive(true);

                transform.SetParent(_startParent);
                transform.localPosition = _start;
                transform.localRotation = _startRotation;

                _isMoving = false;
                _time = 0;
                _target.localPosition = Vector3.zero;

                //ClearTrail();
                _trail.Clear();
                Punched?.Invoke();
            }
        }

        private void OnFistCollided()
        {
            _isMoveBack = true;
        }
    }
}
