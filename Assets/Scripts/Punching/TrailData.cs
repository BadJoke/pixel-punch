﻿using FUGames.Pooling;
using UnityEngine;

namespace Punching
{
    public class TrailData : PoolObjectData
    {
        public Material OwnerMaterial { get; private set; }

        public TrailData(PoolManager poolManager, Material ownerMaterial) : base(poolManager)
        {
            OwnerMaterial = ownerMaterial;
        }
    }
}
