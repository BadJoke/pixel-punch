﻿using FUGames.Pooling;
using UnityEngine;

namespace Punching
{
    public class TrailElement : PoolObject
    {
        [SerializeField] private MeshRenderer _renderer;

        public float Time;

        public override void Init(PoolObjectData data)
        {
            base.Init(data);

            var trailData = data as TrailData;

            _renderer.material = trailData.OwnerMaterial;
        }
    }
}
