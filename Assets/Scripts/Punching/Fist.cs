﻿using Partman;
using UnityEngine;
using UnityEngine.Events;

namespace Punching
{
    [RequireComponent(typeof(Collider))]
    public class Fist : MonoBehaviour
    {
        [SerializeField] private int _damage;
        [SerializeField] private int _obstacleLayer;
        [SerializeField] private int _opponentLayer;
        [SerializeField] private ParticleSystem _hitEffect;

        private Transform _parent;
        private Vector3 _position;
        private Quaternion _rotation;
        private Collider _collider;

        public event UnityAction Collided;

        private void Start()
        {
            _parent = transform.parent;
            _position = transform.localPosition;
            _rotation = transform.localRotation;
            _collider = GetComponent<Collider>();
            _collider.enabled = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == _obstacleLayer)
            {
                Collided?.Invoke();

                if (_hitEffect)
                {
                    Instantiate(_hitEffect, transform.position, Quaternion.identity).Play();
                }

                return;
            }

            if (other.gameObject.layer == _opponentLayer)
            {
                Collided?.Invoke();

                if (_hitEffect)
                {
                    Instantiate(_hitEffect, transform.position, Quaternion.identity).Play();
                }

                if (other.gameObject.TryGetComponent(out DamageTaker damageTaker))
                {
                    damageTaker.Take(_damage, transform);
                    return;
                }
            }
        }

        public void SetDamage(int amount)
        {
            _damage = amount;
        }

        public void Enable()
        {
            _collider.enabled = true;
        }

        public void SetStartParameters()
        {
            transform.SetParent(_parent);
            transform.localPosition = _position;
            transform.localRotation = _rotation;

            _collider.enabled = false;
        }

        public void Fall(Transform parent)
        {
            transform.SetParent(parent);

            _collider.enabled = true;
            _collider.isTrigger = false;

            var rigidbody = GetComponent<Rigidbody>();
            rigidbody.isKinematic = false;
            rigidbody.useGravity = true;
        }
    }
}
