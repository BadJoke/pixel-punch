﻿using FUGames.Pooling;
using System.Collections.Generic;
using UnityEngine;

namespace Punching
{
    public class TrailMaker : MonoBehaviour
    {
        [SerializeField] private float _density = 25f;
        [SerializeField] private Material _ownerMaterial;
        
        private int _index = 0;
        private Vector3 _elementScale = new Vector3(0.25f, 0.25f, 0.25f);
        private TrailData _data;
        private List<TrailElement> _trail = new List<TrailElement>();
        private PoolManager _poolManager;

        private void Start()
        {
            _poolManager = FindObjectOfType<PoolManager>();
            _data = new TrailData(_poolManager, _ownerMaterial);
        }

        public void Spawn(Vector3 start, Vector3 end, Vector3 power, float movingTime)
        {
            var centerOffset = ((start + end) / 2f) + power;
            var punchLength = Vector3.Distance(start, centerOffset) + Vector3.Distance(centerOffset, end);
            var deltaTime = movingTime / (punchLength * _density);

            for (float time = 0; time < movingTime; time += deltaTime)
            {
                var position = Parabola.ParabolaV3(start, end, power, time / movingTime);

                var trailElement = _poolManager.Take(typeof(TrailElement)) as TrailElement;
                trailElement.Init(_data);
                trailElement.Time = time;
                trailElement.transform.SetParent(transform);
                trailElement.transform.localPosition = position;
                trailElement.transform.localScale = _elementScale;
                _trail.Add(trailElement);
            }
        }

        public Transform Scroll(bool isMoveBack, float time)
        {
            if (isMoveBack)
            {
                while (_trail[_index].Time >= time)
                {
                    _trail[_index].gameObject.SetActive(false);

                    if (_index == 0)
                    {
                        break;
                    }

                    _index--;
                }
            }
            else
            {
                while (_trail[_index].Time <= time)
                {
                    _trail[_index].gameObject.SetActive(true);

                    if (_index == _trail.Count - 1)
                    {
                        break;
                    }

                    _index++;
                }
            }

            return _trail[_index].transform;
        }

        public void RotateFist(Transform fist)
        {
            if (_index < _trail.Count - 1)
            {
                fist.LookAt(_trail[_index + 1].transform);
            }
        }

        public void Clear()
        {
            foreach (var element in _trail)
            {
                element.BackToPool();
            }

            _trail.Clear();
        }
    }
}
