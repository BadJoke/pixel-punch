﻿using DG.Tweening;
using LevelColoring;
using Partman;
using Partman.Player;
using System.Collections.Generic;
using UI;
using UnityEngine;

namespace PlateManaging
{
    public class PlateManager : MonoBehaviour
    {
        [SerializeField] ColorSetter _colorSetter;
        [SerializeField] private Menu _menu;
        [SerializeField] private Judge _judge;
        [SerializeField] private Plate[] _plates;
        [SerializeField] private Transform _water;
        [SerializeField] private float _spawnOffset = 25f;
        [SerializeField] private int _visibleCount = 3;
        [SerializeField] private float _growingTime = 0.5f;
        [SerializeField] private CameraReplacer _cameraReplacer;
        [SerializeField] private CameraShaker _cameraShaker;

        private int _level;
        private Player _player;
        private Plate _activePlate; 
        private Plate _lastSpawnedPlate;
        private Queue<Plate> _spawnedPlates = new Queue<Plate>();
        private Vector3 _spawnPosition = Vector3.zero;
        private PlateData _data;
        private UIHandler _uiHandler;

        private void OnEnable()
        {
            _cameraReplacer.Replaced += OnCameraReplaced;
        }

        private void Start()
        {
            _uiHandler = UIHandler.Instance;
            _level = _uiHandler.CurrentLevel;
            _colorSetter.SetEnemy(_level);

            for (int i = 0; i < _visibleCount; i++)
            {
                var index = GetIndex(i);

                Spawn(_plates[index]);
            }

            _data.GrowingTime = _growingTime;
            _data.CameraReplacer = _cameraReplacer;
            _data.CameraShaker = _cameraShaker;
            _data.Menu = _menu;

            _activePlate = _spawnedPlates.Dequeue();
            _activePlate.Activate(_data);
        }

        private void OnDisable()
        {
            _cameraReplacer.Replaced -= OnCameraReplaced;
        }

        public void NextPlate()
        {
            _colorSetter.SetEnemy(_uiHandler.CurrentLevel);

            var index = GetIndex(_visibleCount - 1);

            Spawn(_plates[index]);

            var plate = _spawnedPlates.Dequeue();
            plate.Activate(_data);
            _activePlate.Deactivate();
            _activePlate = plate;
            _water.DOMoveZ(_activePlate.transform.position.z, _cameraReplacer.PlacingTime);
        }

        private void Spawn(Plate prefab)
        {
            if (_spawnedPlates.Count > 0)
            {
                var enemyDistance = _lastSpawnedPlate.EnemyDistance;
                var playerDistance = prefab.PlayerDistance;
                _spawnPosition = _lastSpawnedPlate.transform.position;
                _spawnPosition.z += enemyDistance + _spawnOffset + playerDistance;
            }

            _lastSpawnedPlate = Instantiate(prefab, _spawnPosition, Quaternion.identity);
            _lastSpawnedPlate.Activated += OnPlateActivated;
            _spawnedPlates.Enqueue(_lastSpawnedPlate);
            _colorSetter.SetPlate(_level++, _lastSpawnedPlate);
        }

        private int GetIndex(int i)
        {
            return (i + _uiHandler.CurrentLevel - 1) % _plates.Length;
        }

        private void OnPlateActivated(Player player, TargetQueue playerTargets, TargetQueue enemyTargets)
        {
            _player = player;
            _judge.Init(playerTargets, enemyTargets);
        }

        private void OnCameraReplaced()
        {
            _player.EnableSwiping();
        }
    }
}
