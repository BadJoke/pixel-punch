﻿using UI;

namespace PlateManaging
{
    public struct PlateData
    {
        public float GrowingTime;
        public CameraReplacer CameraReplacer;
        public CameraShaker CameraShaker;
        public Menu Menu;
    }
}
