﻿using DG.Tweening;
using Partman;
using Partman.Enemy;
using Partman.Player;
using UnityEngine;
using UnityEngine.Events;

namespace PlateManaging
{
    public class Plate : MonoBehaviour
    {
        [SerializeField] private float _jellyTime = 0.5f;
        [SerializeField] private Player _player;
        [SerializeField] private Enemy _enemy;
        [SerializeField] private Transform _playerPoint;
        [SerializeField] private Transform _enemyPoint;
        [SerializeField] private MeshRenderer[] _renderers;

        private const float BoxerSize = 4.954789f;

        public MeshRenderer[] Renderers => _renderers;
        public float PlayerDistance => Mathf.Abs(_playerPoint.localPosition.z);
        public float EnemyDistance => Mathf.Abs(_enemyPoint.localPosition.z);

        public event UnityAction<Player, TargetQueue, TargetQueue> Activated;

        private void Start()
        {
            transform.localScale = Vector3.zero;

            var sequence = DOTween.Sequence();
            sequence.Append(transform.DOScale(1.3f, _jellyTime * 0.8f));
            sequence.Append(transform.DOScale(1f, _jellyTime * 0.2f));
        }

        public void Activate(PlateData data)
        {
            var player = Instantiate(_player, _playerPoint.position, Quaternion.identity);
            var enemy = Instantiate(_enemy, _enemyPoint.position, Quaternion.Euler(0f, 180f, 0f));
            var playerTargets = player.GetComponentInChildren<TargetQueue>();
            var enemyTargets = enemy.GetComponentInChildren<TargetQueue>();

            player.Init(enemyTargets, data.CameraReplacer, data.Menu);
            enemy.Init(playerTargets, data.CameraShaker, data.Menu);

            var playerModel = player.transform.GetChild(0);
            var enemyModel = enemy.transform.GetChild(0);

            playerModel.transform.localScale = Vector3.zero;
            enemyModel.transform.localScale = Vector3.zero;

            playerModel.transform.DOScale(BoxerSize, data.GrowingTime).onComplete += () =>
            {
                player.transform.SetParent(transform);
            };
            enemyModel.transform.DOScale(BoxerSize, data.GrowingTime).onComplete += () =>
            {
                enemy.transform.SetParent(transform);
            };

            Activated?.Invoke(player, playerTargets, enemyTargets);
        }

        public void Deactivate()
        {
            transform.DOScale(0f, _jellyTime).onComplete += () =>
            { 
                Destroy(gameObject);
            };
        }
    }
}
