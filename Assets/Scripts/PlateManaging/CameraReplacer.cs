﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

namespace PlateManaging
{
    public class CameraReplacer : MonoBehaviour
    {
        [SerializeField] private Vector3 _localPosition;
        [SerializeField] private Vector3 _localRotation;
        [SerializeField] private float _placingTime;

        public float PlacingTime => _placingTime;
        public Vector3 LocalPosition => _localPosition;

        public event UnityAction Replaced;

        public void Init(Transform parent)
        {
            transform.SetParent(parent);

            transform.DOLocalMove(_localPosition, _placingTime);
            transform.DOLocalRotate(_localRotation, _placingTime).onComplete += () =>
            {
                Replaced?.Invoke();
            };
        }
    }
}
