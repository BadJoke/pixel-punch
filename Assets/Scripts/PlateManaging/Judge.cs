﻿using Partman;
using UI;
using UnityEngine;

namespace PlateManaging
{
    public class Judge : MonoBehaviour
    {
        [SerializeField] private UIHandler _uiHandler;

        private TargetQueue _player;
        private TargetQueue _enemy;

        public void Init(TargetQueue player, TargetQueue enemy)
        {
            _player = player;
            _enemy = enemy;

            _player.Destroyed += OnPlayerDestroyed;
            _enemy.Destroyed += OnEnemyDestroyed;
        }

        private void Unsubscribe()
        {
            _player.Destroyed -= OnPlayerDestroyed;
            _enemy.Destroyed -= OnEnemyDestroyed;
        }

        private void OnPlayerDestroyed()
        {
            Unsubscribe();

            _uiHandler.Lose();
        }

        private void OnEnemyDestroyed()
        {
            Unsubscribe();
            
            _uiHandler.Win();
        }
    }
}
