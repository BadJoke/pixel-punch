﻿using PlateManaging;
using System.Collections;
using UnityEngine;

public class CameraShaker : MonoBehaviour
{
    [SerializeField] private bool _canShake;
    [SerializeField] private float _duration = 0.5f;
    [SerializeField] private float _magnitude = 0.5f;

    private Vector3 _originalPosition;

    private void Start()
    {
        _originalPosition = GetComponent<CameraReplacer>().LocalPosition;
    }

    public void Shake()
    {
        if (_canShake)
        {
            StartCoroutine(Shaking());
        }
    }

    private IEnumerator Shaking()
    {
        float elapsed = 0f;

        while (elapsed < _duration)
        {
            float x = Random.Range(-1f, 1f) * _magnitude;
            float y = Random.Range(-1f, 1f) * _magnitude;

            transform.localPosition = _originalPosition + new Vector3(x, y, 0f);

            elapsed += Time.deltaTime;

            yield return null;
        }

        transform.localPosition = _originalPosition;
    }
}
