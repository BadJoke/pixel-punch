﻿using UnityEngine;

public static class Parabola
{
    public static Vector3 ParabolaV3(Vector3 start, Vector3 end, Vector3 power, float t)
    {
        var parabola = Vector3.Lerp(start, end, t);
        parabola.x += Calculate(power.x, t);
        parabola.y += Calculate(power.y, t);
        parabola.z += Calculate(power.z, t);

        return parabola;
    }

    private static float Calculate(float power, float t)
    {
        return -4 * power * t * t + 4 * power * t;
    }
}
