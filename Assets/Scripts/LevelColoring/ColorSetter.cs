﻿using PlateManaging;
using UI;
using UnityEngine;

namespace LevelColoring
{
    public class ColorSetter : MonoBehaviour
    {
        [SerializeField] private int _kitLevelsCount = 4;

        [Header("Enemy")]
        [SerializeField] private Material _enemyBody;
        [SerializeField] private Material _enemyGlows1;
        [SerializeField] private Material _enemyGlows2;
        [SerializeField] private Material _shield1;
        [SerializeField] private Material _shield2;

        [Header("Bosses")]
        [Header("Angty smile")]
        [SerializeField] private Material _smileBody;
        [SerializeField] private Material _smileFace;
        [SerializeField] private Material _smileBrows;

        [Header("Eye of Ctulhu")]
        [SerializeField] private Material _ctulhuBody;
        [SerializeField] private Material _ctulhuIris;

        [Header("Sauron tower")]
        [SerializeField] private Material _towerRocks;
        [SerializeField] private Material _towerMain;
        [SerializeField] private Material _towerIris;

        [Header("Progress bar")]
        [SerializeField] private Level[] _levels;
        //[SerializeField] private SpriteRenderer _coinsBackgound;

        [Header("Color sets")]
        [SerializeField] private ColorKit[] _sets;

        private const string Color = "_Color";
        private const string Emission = "_Emission";

        public void SetEnemy(int level)
        {
            var kit = GetColorKit(level);

            SetMaterialColor(kit.EnemyBody, _enemyBody);
            SetMaterialColor(kit.EnemyGlows1, _enemyGlows1);
            SetMaterialColor(kit.EnemyGlows2, _enemyGlows2);
            SetMaterialColor(kit.Shield1, _shield1);
            SetMaterialColor(kit.Shield2, _shield2);

            SetMaterialColor(kit.SmileBody, _smileBody);
            SetMaterialColor(kit.SmileFace, _smileFace);
            SetMaterialColor(kit.SmileBrows, _smileBrows);

            SetMaterialColor(kit.CtulhuBody, _ctulhuBody);
            SetMaterialColor(kit.CtulhuIris, _ctulhuIris);

            SetMaterialColor(kit.TowerRocks, _towerRocks);
            SetMaterialColor(kit.TowerMain, _towerMain);
            SetMaterialColor(kit.TowerIris, _towerIris);

            foreach (var uiLevel in _levels)
            {
                uiLevel.InitColors(kit);
            }

            //_coinsBackgound.color = kit.InnerPassed;
        }

        public void SetPlate(int level, Plate plate)
        {
            var set = GetColorKit(level);
            var coloringObjects = plate.GetComponentsInChildren<ColoringObject>();

            foreach (var coloringObject in coloringObjects)
            {
                coloringObject.SetColors(set);
            }

            foreach (var renderer in plate.Renderers)
            {
                renderer.material = set.CheckmateFloor;
            }
        }

        private ColorKit GetColorKit(int level)
        {
            var index = (level - 1) / _kitLevelsCount;
            
            while (index >= _sets.Length)
            {
                index -= _sets.Length;
            }

            return _sets[index];
        }

        private void SetMaterialColor(Material from, Material to)
        {
            to.SetColor(Color, from.GetColor(Color));
            to.SetColor(Emission, from.GetColor(Emission));
        }
    }
}