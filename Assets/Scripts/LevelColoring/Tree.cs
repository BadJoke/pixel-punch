﻿using UnityEngine;

namespace LevelColoring
{
    public class Tree : ColoringObject
    {
        [SerializeField] private MeshRenderer _trunk;
        [SerializeField] private MeshRenderer[] _meshes1;
        [SerializeField] private MeshRenderer[] _meshes2;

        public override void SetColors(ColorKit set)
        {
            _trunk.material = set.TreeWood;

            foreach (var mesh in _meshes1)
            {
                var materials = mesh.materials;

                materials[0] = set.TreeLeaves;
                materials[1] = set.TreeWood;

                mesh.materials = materials;
            }

            foreach (var mesh in _meshes2)
            {
                var materials = mesh.materials;

                materials[0] = set.TreeWood;
                materials[1] = set.TreeLeaves;

                mesh.materials = materials;
            }
        }
    }
}
