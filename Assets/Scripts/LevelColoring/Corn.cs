﻿using UnityEngine;

namespace LevelColoring
{
    public class Corn : ColoringObject
    {
        [SerializeField] private MeshRenderer _trunk;
        [SerializeField] private MeshRenderer[] _meshes1;
        [SerializeField] private MeshRenderer[] _meshes2;

        public override void SetColors(ColorKit set)
        {
            _trunk.material = set.CornTrunk;

            foreach (var mesh in _meshes1)
            {
                var materials = mesh.materials;

                materials[0] = set.CornInternal;
                materials[1] = set.CornExternal;
                materials[2] = set.CornMiddle;

                mesh.materials = materials;
            }

            foreach (var mesh in _meshes2)
            {
                var materials = mesh.materials;

                materials[0] = set.CornInternal;
                materials[1] = set.CornMiddle;
                materials[2] = set.CornExternal;

                mesh.materials = materials;
            }
        }
    }
}
