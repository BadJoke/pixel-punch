﻿using UnityEngine;

namespace LevelColoring
{
    [CreateAssetMenu(fileName = "Kit 0-0")]
    public class ColorKit : ScriptableObject
    {
        [Header("Enemy")]
        public Material EnemyBody;
        public Material EnemyGlows1;
        public Material EnemyGlows2;
        public Material Shield1;
        public Material Shield2;

        [Header("Bosses")]
        [Header("Angty smile")]
        public Material SmileBody;
        public Material SmileFace;
        public Material SmileBrows;

        [Header("Eye of Ctulhu")]
        public Material CtulhuBody;
        public Material CtulhuIris;

        [Header("Sauron tower")]
        public Material TowerRocks;
        public Material TowerMain;
        public Material TowerIris;

        [Header("Floor")]
        public Material CheckmateFloor;

        [Header("Box")]
        public Material BoxExternal;
        public Material BoxMiddle;
        public Material BoxInternal;

        [Header("Corn")]
        public Material CornTrunk;
        public Material CornExternal;
        public Material CornMiddle;
        public Material CornInternal;

        [Header("Tree")]
        public Material TreeWood;
        public Material TreeLeaves;

        [Header("Progress bar")]
        public Color Outer;
        public Color Inner;
        public Color OuterPassed;
        public Color InnerPassed;
    }
}
