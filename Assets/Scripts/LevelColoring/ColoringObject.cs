﻿using UnityEngine;

namespace LevelColoring
{
    public abstract class ColoringObject : MonoBehaviour
    {
        public abstract void SetColors(ColorKit set);
    }
}
