﻿using UnityEngine;

namespace LevelColoring
{
    public class Box : ColoringObject
    {
        public override void SetColors(ColorKit set)
        {
            var meshes = GetComponentsInChildren<MeshRenderer>();

            foreach (var mesh in meshes)
            {
                var materials = mesh.materials;

                materials[0] = set.BoxInternal;
                materials[1] = set.BoxExternal;
                materials[2] = set.BoxMiddle;

                mesh.materials = materials;
            }
        }
    }
}
