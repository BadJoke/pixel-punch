﻿using UnityEngine;

namespace UI
{
    public class RestartButton : MonoBehaviour
    {
        [SerializeField] private Animator _animator;

        public void Show()
        {
            _animator.SetTrigger("Show");
        }

        public void Hide()
        {
            _animator.SetTrigger("Hide");
        }

        public void OnRestartClick()
        {
            UIHandler.Instance.Reload();
        }
    }
}
