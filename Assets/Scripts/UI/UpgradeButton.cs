﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UpgradeButton : MonoBehaviour
    {
        [SerializeField] private Text _rateLevel;
        [SerializeField] private Text _price;
        [SerializeField] private GameObject _inactivitySubstrate;
        [SerializeField] private string _parametr;
        [SerializeField] private UpgradeButton _pair;
        [SerializeField] private Wallet _wallet;

        private int _priceAmount;

        private void Start()
        {
            var rate = PlayerPrefs.HasKey(_parametr) ? PlayerPrefs.GetInt(_parametr) : 1;

            UpdateValues(rate);
        }

        public void UpdateValues(int rate)
        {
            _priceAmount = Economic.GetUpgradePrice(rate);

            _rateLevel.text = "LV. " + rate;
            _price.text = _priceAmount.ToString();

            CheckInteractivity();
            _pair.CheckInteractivity();
        }

        public void CheckInteractivity()
        {
            _inactivitySubstrate.SetActive(_wallet.Amount < _priceAmount);
        }
    }
}