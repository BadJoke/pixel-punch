﻿using DG.Tweening;
using DG.Tweening.Core;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class Wallet : MonoBehaviour
    {
        [SerializeField] private Text _amountDisplay;
        [SerializeField] private float _changingTime;

        private int _coinsAmount;
        private DOGetter<int> _getter;
        private DOSetter<int> _setter;

        private const string Coins = "Coins";

        public int Amount { get; private set; }

        private void Awake()
        {
            _coinsAmount = PlayerPrefs.HasKey(Coins) ? PlayerPrefs.GetInt(Coins) : 0;
            Amount = _coinsAmount;

            _getter = Getter;
            _setter = Setter;

            _amountDisplay.text = _coinsAmount.ToString();
        }

        public void AddCoins(int amount)
        {
            Amount = _coinsAmount + amount;
            DOTween.To(_getter, _setter, _coinsAmount + amount, _changingTime).onComplete += SaveCoins;
        }

        public void RemoveCoins(int amount)
        {
            Amount = _coinsAmount - amount;
            DOTween.To(_getter, _setter, _coinsAmount - amount, _changingTime).onComplete += SaveCoins;
        }

        private int Getter()
        {
            return _coinsAmount;
        }

        private void Setter(int amount)
        {
            _coinsAmount = amount;
            _amountDisplay.text = _coinsAmount.ToString();
        }

        private void SaveCoins()
        {
            PlayerPrefs.SetInt(Coins, _coinsAmount);
            PlayerPrefs.Save();
        }
    }
}
