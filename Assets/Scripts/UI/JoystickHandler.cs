﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class JoystickHandler : MonoBehaviour
    {
        [SerializeField] private Image _joystickOutter;
        [SerializeField] private Image _joystickInner;

        private float _size;
        private GameObject _joystick;
        private Vector2 _inputVector;

        private void Start()
        {
            _size = _joystickOutter.rectTransform.sizeDelta.x;
            _joystick = _joystickOutter.gameObject;
            _joystick.SetActive(false);
        }

        public void Show(Vector2 position)
        {
            _joystickOutter.transform.position = position;
            _joystick.SetActive(true);
        }

        public void Hide()
        {
            _joystick.SetActive(false);
            _joystickInner.rectTransform.anchoredPosition = Vector2.zero;
        }

        private void StartPosition(Vector2 pos)
        {
            _joystickOutter.rectTransform.position = new Vector2(pos.x, pos.y);
        }

        public virtual void OnDrag(Vector2 dragPosition)
        {
            Vector2 position;

            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_joystickOutter.rectTransform, dragPosition, null, out position))
            {
                position /= _size;

                _inputVector = position * 2;

                if (_inputVector.magnitude > 1f)
                {
                    _inputVector.Normalize();
                }

                var anchoredPosition = _inputVector * (_size / 2.75f);
                _joystickInner.rectTransform.anchoredPosition = anchoredPosition;
            }
        }

        public void OnPointerDown(Vector2 position)
        {
            StartPosition(position);
        }

        public void OnPointerUp()
        {
            _inputVector = Vector2.zero;
            _joystickInner.rectTransform.anchoredPosition = Vector2.zero;
        }
    }
}
