﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI
{
    public class UpgradePanel : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private GameObject _prompt;
        [SerializeField] private RectTransform _buttons;
        [SerializeField] private RectTransform _coins;
        [SerializeField] private Upgrader _upgradeMenu;

        public void Activate()
        {
            _upgradeMenu.Refresh();

            gameObject.SetActive(true);

            _buttons.DOAnchorPosY(0f, 0.15f);
            _coins.DOAnchorPosX(-124f, 0.15f);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            _prompt.SetActive(true);

            _buttons.DOAnchorPosY(-540f, 0.15f);
            _coins.DOAnchorPosX(150f, 0.15f).onComplete += () =>
            {
                gameObject.SetActive(false);
            };
        }
    }
}
