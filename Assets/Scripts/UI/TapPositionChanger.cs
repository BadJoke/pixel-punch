﻿using UnityEngine;

public class TapPositionChanger
{
    private RectTransform _transform;
    private float _halfWidth;
    private float _halfHeight;

    public TapPositionChanger(RectTransform transform)
    {
        _transform = transform;
        _halfWidth = transform.rect.width / 2f;
        _halfHeight = transform.rect.height / 2f;
    }

    public Vector2 RandomPosition()
    {
        var x = Random.Range(-_halfWidth, _halfWidth);
        var y = Random.Range(-_halfHeight, _halfHeight);
        var randomedPosition = new Vector3(x, y);
        return randomedPosition + _transform.localPosition;
    }
}
