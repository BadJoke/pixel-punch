﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UI
{
    public class MovingZone : MonoBehaviour, IPointerDownHandler, IDragHandler, IEndDragHandler, IPointerUpHandler
    {
        public event UnityAction<Vector2> MoveStarted;
        public event UnityAction<Vector2> Moving;
        public event UnityAction Moved;

        public void OnPointerDown(PointerEventData eventData)
        {
            MoveStarted?.Invoke(eventData.position);
        }

        public void OnDrag(PointerEventData eventData)
        {
            Moving?.Invoke(eventData.position);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            Moved?.Invoke();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Moved?.Invoke();
        }
    }
}
