﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class TapEffect : MonoBehaviour
{
    [SerializeField] private RectTransform _zone;
    [SerializeField] private RectTransform _tapRect;
    [SerializeField] private Image _tapImage;
    [SerializeField] private Image _pointerImage;
    [SerializeField] private bool _canRandomChangePosition;

    private Sequence _sequence;
    private TapPositionChanger _changer;

    private void Start()
    {
        _changer = new TapPositionChanger(_zone);

        SetSequence();

        if (_canRandomChangePosition)
        {
            transform.localPosition = _changer.RandomPosition();

            _sequence.onStepComplete += RandomChagingPosition;
        }
    }

    public void Stop()
    {
        _sequence.Kill();
        gameObject.SetActive(false);
    }

    private void SetSequence()
    {
        _sequence = DOTween.Sequence().SetLoops(-1).SetUpdate(true);
        _sequence.Append(_pointerImage.DOFade(1f, 0.35f));
        _sequence.Join(_tapRect.DOSizeDelta(Vector2.one, 0.5f));
        _sequence.Join(_tapRect.DOScale(Vector3.one * 0.75f, 0.5f));
        _sequence.Append(_pointerImage.DOFade(0f, 0.67f));
        _sequence.Join(_tapRect.DOSizeDelta(Vector2.one * 175f, 1f));
        _sequence.Join(_tapRect.DOScale(Vector3.one * 1.5f, 1f));
        _sequence.Join(_tapImage.DOFade(0f, 1f));
    }

    private void RandomChagingPosition()
    {
        transform.localPosition = _changer.RandomPosition();
    }
}
