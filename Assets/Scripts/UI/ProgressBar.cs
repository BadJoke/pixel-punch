﻿using UnityEngine;

namespace UI
{
    public class ProgressBar : MonoBehaviour
    {
        [SerializeField] private Level[] _levels;

        private int _currentLevel;

        private void Awake()
        {
            _levels = GetComponentsInChildren<Level>();
        }

        public void Init(int level)
        {
            var difference = level % _levels.Length;

            if (difference == 0)
            {
                difference = _levels.Length;
            }

            var minLevel = level - difference + 1;
            var maxLevel = minLevel + _levels.Length;

            _currentLevel = difference - 1;

            for (int i = minLevel; i < maxLevel; i++)
            {
                var passed = i < level - 1 ? 1f : i == level - 1 ? 0.5f : 0f;
                _levels[i - minLevel].Init(i, passed);
            }
        }

        public void LevelPassed()
        {
            _levels[_currentLevel++].Pass();
        }
    }
}
