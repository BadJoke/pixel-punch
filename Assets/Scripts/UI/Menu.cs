﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UI
{
    public class Menu : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] private GameObject _prompt;
        [SerializeField] private PunchZone _punchZone;
        [SerializeField] private MovingZone _movingZone;

        public event UnityAction GameStarted;

        private void OnEnable()
        {
            _punchZone.Punching += OnPunching;
            _movingZone.MoveStarted += OnMoveStarted;
        }

        private void OnDisable()
        {
            _punchZone.Punching -= OnPunching;
            _movingZone.MoveStarted -= OnMoveStarted;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            GameStarted?.Invoke();
            _prompt.SetActive(false);
            gameObject.SetActive(false);
        }

        private void OnPunching(float x)
        {
            OnPointerDown(null);
        }

        private void OnMoveStarted(Vector2 vector)
        {
            OnPointerDown(null);
        }
    }
}
