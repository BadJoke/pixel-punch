﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class EndPanel : MonoBehaviour
    {
        [SerializeField] private Wallet _wallet;
        [SerializeField] private Image _background;
        [SerializeField] private Text _button;
        [SerializeField] private Text _message;
        [SerializeField] private Text _reward;
        [SerializeField] private Text _coins;
        [SerializeField] private Image _coinsImage;
        [SerializeField] private RectTransform _coinsParent;
        [SerializeField] private RectTransform _coinsBackground;
        [SerializeField] private float _showingTime = 0.5f;
        [SerializeField] private float _messsageHeightOffset = -300f;

        private bool _hasWin;
        private UIHandler _handler;
        private Vector2 _messagePosition;

        private const string WinMessage = "LEVEL {0} COMPLETED!";
        private const string WinButtonText = "NEXT";
        private const string LoseMessage = "FAILED!";
        private const string LoseButtonText = "RESTART";

        private void Start()
        {
            _messagePosition = _message.rectTransform.anchoredPosition;
        }

        public void Show(bool hasWin)
        {
            _handler = UIHandler.Instance;
            _hasWin = hasWin;

            gameObject.SetActive(true);

            if (_hasWin)
            {
                SetValues(WinMessage, WinButtonText);
            }
            else
            {
                SetValues(LoseMessage, LoseButtonText);

                _background.DOFade(50f / 256f, _showingTime);
            }

            _message.rectTransform.DOAnchorPosY(_messsageHeightOffset, _showingTime).SetUpdate(true);

            DOTween.Sequence().SetUpdate(true)
                .Append(_button.transform.parent.DOScale(1.2f, _showingTime * 0.8f))
                .Append(_button.transform.parent.DOScale(1f, _showingTime * 0.2f));

            _coinsBackground.DOScaleX(0f, 0f);
            _coinsParent.DOScale(5f, 0f);
            _coins.DOFade(0f, 0f);
            _coinsImage.DOFade(0f, 0f);

            DOTween.Sequence().SetUpdate(true)
                .Append(_coinsBackground.DOScaleX(1f, 0.5f))
                .Append(_coinsParent.DOScale(1f, 0.5f))
                .Join(_coins.DOFade(1f, 0.5f))
                .Join(_coinsImage.DOFade(1f, 0.5f));
        }

        private void Refresh()
        {
            Time.timeScale = 1f;

            DOTween.Sequence()
                .Join(_button.transform.parent.DOScale(0f, 0.05f))
                .Join(_coins.DOFade(0f, 0.15f))
                .Join(_coinsImage.DOFade(0f, 0.15f))
                .Append(_coinsBackground.DOScaleX(0f, 0.15f))
                .onComplete += () =>
                {
                    _handler.NextLevel();

                    gameObject.SetActive(false);

                    _message.rectTransform.anchoredPosition = _messagePosition;
                };
        }

        private void SetValues(string message, string buttonText)
        {
            _message.text = _hasWin ? string.Format(message, _handler.CurrentLevel) : message;
            _button.text = buttonText;

            var reward = Economic.GetReward(_hasWin, _handler.CurrentLevel);

            _reward.text = reward.ToString();
            _wallet.AddCoins(reward);
        }

        public void OnButtonClick()
        {
            if (_hasWin)
            {
                Refresh();
            }
            else
            {
                transform.DetachChildren();
                transform.SetAsLastSibling();

                _background.DOFade(1f, 0.3f).onComplete += () =>
                {
                    _handler.Reload();
                };
            }
        }
    }
}
