﻿using LevelColoring;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class Level : MonoBehaviour
    {
        [SerializeField] private Image _outer;
        [SerializeField] private Image _inner;
        [SerializeField] private Text _number;
        [SerializeField] private Image _preLine;
        [SerializeField] private Image _pastLine;
        [SerializeField] private Image _pastLineOuter;

        private Color _outerColor;
        private Color _innerColor;
        private Color _outerColorPassed;
        private Color _innerColorPassed;

        public void Init(int level, float passed)
        {
            _number.text = level.ToString();

            if (passed > 0f)
            {
                SetColors(passed, _innerColorPassed, _outerColorPassed);
            }
            else
            {
                SetColors(passed, _innerColor, _outerColor);
            }
        }

        public void InitColors(ColorKit set)
        {
            _innerColor = set.Inner;
            _outerColor = set.Outer;
            _innerColorPassed = set.InnerPassed;
            _outerColorPassed = set.OuterPassed;

            if (_pastLine)
            {
                _pastLine.color = _innerColorPassed;
                _pastLineOuter.color = _outerColorPassed;
            }
        }

        public void Pass()
        {
            if (_preLine)
            {
                _preLine.fillAmount = 1f;
            }

            _outer.color = _outerColorPassed;
            _inner.color = _innerColorPassed;
        }

        private void SetColors(float passed, Color inner, Color outer)
        {
            _inner.color = inner;
            _outer.color = outer;

            if (_pastLine)
            {
                _pastLine.fillAmount = passed;
            }
        }
    }
}
