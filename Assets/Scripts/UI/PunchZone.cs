﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UI
{
    public class PunchZone : MonoBehaviour, IPointerClickHandler
    {
        public event UnityAction<float> Punching;

        public void OnPointerClick(PointerEventData eventData)
        {
            Punching?.Invoke(eventData.position.x);
        }
    }
}
