﻿using DG.Tweening;
using DG.Tweening.Plugins.Core.PathCore;
using UnityEngine;

namespace UI
{
    public class PointerMovement : MonoBehaviour
    {
        [SerializeField] private RectTransform[] _targets;
        [SerializeField] private float _movingTime = 4f;

        private Vector3[] _targetPositions;
        private Tween _tween;

        private void OnEnable()
        {
            Move();
        }

        private void OnDisable()
        {
            _tween?.Kill();
        }

        private void Move()
        {
            transform.localPosition = Vector3.zero;

            _targetPositions = new Vector3[_targets.Length];

            for (int i = 0; i < _targets.Length; i++)
            {
                _targetPositions[i] = _targets[i].position;
            }

            var path = new Path(PathType.CatmullRom, _targetPositions, _targetPositions.Length);
            _tween = transform.DOPath(path, _movingTime);
            _tween.SetLoops(-1).SetDelay(0.5f);
        }
    }
}
