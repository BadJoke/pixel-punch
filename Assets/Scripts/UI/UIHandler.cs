﻿using PlateManaging;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Stopwatch = System.Diagnostics.Stopwatch;

namespace UI
{
    public class UIHandler : MonoBehaviour
    {
        [SerializeField] private PunchZone _punchZone;
        [SerializeField] private MovingZone _movingZone;
        [SerializeField] private ProgressBar _progressBar;
        [SerializeField] private RestartButton _restartButton;
        [SerializeField] private Menu _menu;
        [SerializeField] private UpgradePanel _upgradePanel;
        [SerializeField] private EndPanel _panel;
        [SerializeField] private PlateManager _plateManager;
        [SerializeField] private ParticleSystem[] _confetti;
        [SerializeField] private float _slowMotionScale = 0.5f;
        [SerializeField] private float _slowMotionDuration = 1f;

        private Stopwatch _stopwatch;

        private const string Level = "Level";

        private LevelFinishedResult finishedResult;

        public int CurrentLevel { get; private set; }
        public PunchZone PunchZone => _punchZone;
        public MovingZone MovingZone => _movingZone;

        public static UIHandler Instance { get; private set; }

        private void OnEnable()
        {
            _menu.GameStarted += OnGameStarted;
        }

        private void Awake()
        {
            Instance = this;

            Input.multiTouchEnabled = false;

            if (PlayerPrefs.HasKey(Level))
            {
                CurrentLevel = PlayerPrefs.GetInt(Level);
            }
            else
            {
                CurrentLevel = 1;
            }
        }

        private void Start()
        {
            _progressBar.Init(CurrentLevel);

            _stopwatch = new Stopwatch();
        }

        private void OnDisable()
        {
            _menu.GameStarted -= OnGameStarted;
        }

        public void Win()
        {
            ShowPanel(true);

            LevelPassed();
            StartCoroutine(SlowMotion());

            foreach (var confetti in _confetti)
            {
                confetti.Play();
            }
        }

        public void Lose()
        {
            ShowPanel(false);
        }

        public void NextLevel()
        {
            foreach (var confetti in _confetti)
            {
                confetti.Stop();
                confetti.Clear();
            }

            _menu.gameObject.SetActive(true);
            _upgradePanel.Activate();
            _plateManager.NextPlate();
            _progressBar.Init(CurrentLevel);
        }

        public void Reload()
        {
            if (_stopwatch.IsRunning)
            {
                _stopwatch.Stop();

                var playtime = _stopwatch.Elapsed.Seconds;
                finishedResult = LevelFinishedResult.manual_restart;
                HoopslyIntegration.Instance.RaiseLevelFinishedEvent("Level " + CurrentLevel.ToString(), finishedResult, playtime);
                Debug.Log("Level: " + CurrentLevel + " Result: " + finishedResult.ToString() + " Time: " + playtime);
            }

            DG.Tweening.DOTween.Clear();

            var scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }

        private void ShowPanel(bool hasWin)
        {
            _restartButton.Hide();
            _panel.Show(hasWin);
            _stopwatch.Stop();

            var playtime = _stopwatch.Elapsed.Seconds;
            if (hasWin)
            {
                finishedResult = LevelFinishedResult.win;
            }
            else
            {
                finishedResult = LevelFinishedResult.lose;
            }
            HoopslyIntegration.Instance.RaiseLevelFinishedEvent("Level " + CurrentLevel.ToString(), finishedResult, playtime);
            Debug.Log("Level: " + CurrentLevel + " Result: " + finishedResult.ToString() + " Time: " + playtime);
        }

        private void LevelPassed()
        {
            PlayerPrefs.SetInt(Level, ++CurrentLevel);
            _progressBar.LevelPassed();
        }

        private IEnumerator SlowMotion()
        {
            Time.timeScale = _slowMotionScale;

            yield return new WaitForSecondsRealtime(_slowMotionDuration);

            Time.timeScale = 1f;
        }

        private void OnGameStarted()
        {
            _restartButton.Show();
            _stopwatch.Reset();
            _stopwatch.Start();

            Debug.Log("Started " + CurrentLevel + " level");
            HoopslyIntegration.Instance.RaiseLevelStartEvent("Level " + CurrentLevel.ToString());
        }
    }
}
