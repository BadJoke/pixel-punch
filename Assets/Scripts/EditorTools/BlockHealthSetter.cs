﻿using Partman.Blocking;
using UnityEngine;

namespace EditorTools
{
    public class BlockHealthSetter : MonoBehaviour
    {
        [SerializeField] private int _health;
        [SerializeField] private byte _priority;

        public void SetHealth()
        {
            var blocks = GetComponentsInChildren<BlockHealth>();

            foreach (var block in blocks)
            {
                block.SetHealth(_health);
            }
        }

        public void SetPriority()
        {
            var blocks = GetComponentsInChildren<BlockHealth>();

            foreach (var block in blocks)
            {
                block.SetPriority(_priority);
            }
        }
    }
}
