﻿using UnityEngine;
using UnityEngine.Rendering;

namespace EditorTools
{
    public class MeshRendererTool : MonoBehaviour
    {
        public void DisableShadows()
        {
            var renderers = FindObjectsOfType<MeshRenderer>();

            foreach (var renderer in renderers)
            {
                renderer.shadowCastingMode = ShadowCastingMode.Off;
                renderer.receiveShadows = false;
            }
        }
    }
}
