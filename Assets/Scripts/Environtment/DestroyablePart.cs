﻿using UnityEngine;
using UnityEngine.Events;

namespace Environtment
{
    [RequireComponent(typeof(MeshCollider), typeof(Rigidbody))]
    public class DestroyablePart : MonoBehaviour
    {
        [SerializeField] private LayerMask _destoyerLayers;

        private ParticleSystem[] _crumblings;
        private Material[] _materials;

        public event UnityAction<DestroyablePart> Destroyed;

        private void Start()
        {
            _crumblings = GetComponentsInChildren<ParticleSystem>();
            _materials = GetComponent<MeshRenderer>().materials;
        }

        private void OnTriggerEnter(Collider collider)
        {
            if (_destoyerLayers == (_destoyerLayers | 1 << collider.gameObject.layer))
            {
                for (int i = 0; i < _crumblings.Length; i++)
                {
                    var renderer = _crumblings[i].GetComponent<ParticleSystemRenderer>();

                    renderer.material.CopyPropertiesFromMaterial(_materials[i]);

                    _crumblings[i].transform.SetParent(null);
                    _crumblings[i].Play();
                }

                Destroyed?.Invoke(this);

                gameObject.SetActive(false);
            }
        }
    }
}
