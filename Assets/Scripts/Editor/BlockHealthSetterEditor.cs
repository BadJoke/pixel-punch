﻿using UnityEditor;
using UnityEngine;

namespace EditorTools
{
    [CustomEditor(typeof(BlockHealthSetter))]
    public class BlockHealthSetterEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var setter = (BlockHealthSetter)target;

            if (GUILayout.Button("Set health"))
            {
                setter.SetHealth();
            }

            if (GUILayout.Button("Set priority"))
            {
                setter.SetPriority();
            }
        }
    }
}
