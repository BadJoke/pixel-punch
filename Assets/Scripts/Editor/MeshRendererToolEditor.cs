﻿using UnityEditor;
using UnityEngine;

namespace EditorTools
{
    [CustomEditor(typeof(MeshRendererTool))]
    public class MeshRendererToolEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var tool = (MeshRendererTool)target;

            if (GUILayout.Button("Disavle shadows"))
            {
                tool.DisableShadows();
            }
        }
    }
}
